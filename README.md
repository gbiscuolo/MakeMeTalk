# Giovanni Biscuolo Talks

Finally a repository for my rare talks

## Requirements

If you want compile talks from sources you will need:

* [pandoc](http://pandoc.org)
* [GNU Make](https://www.gnu.org/software/make/manual/make.html)

## Talks content

Except otherwhise stated in sources, all **talks** are licensed under the [Creative Commons Attribution-ShareAlike 4.0 International] (https://creativecommons.org/licenses/by-sa/4.0/legalcode) license.

## Code

Except otherwhise stated in sources, all **code** is licensed under the [GNU General Public License v.3.0](https://www.gnu.org/licenses/gpl-3.0.en.html)
